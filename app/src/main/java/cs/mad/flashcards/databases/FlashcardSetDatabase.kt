package cs.mad.flashcards.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao


@Database(entities = [FlashcardSet::class], version = 1)
abstract class FlashcardSetDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "FLASHCARD_SET_DATABASE"
    }

    abstract fun setDao(): FlashcardSetDao
}