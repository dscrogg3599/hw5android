package cs.mad.flashcards.entities

import androidx.room.*

data class FlashcardContainer(
    val flashcards: List<Flashcard>
)

@Entity
data class Flashcard (
    var term: String,
    var definition: String
        ) {
    @PrimaryKey (
        autoGenerate = true
            ) var id: Int = 0
}


@Dao
interface FlashcardDao {
    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(fc: Flashcard)

    @Insert
    suspend fun insertAll(fcs: List<Flashcard>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(fc: Flashcard)

    @Delete
    suspend fun delete(fc: Flashcard)

    @Query("delete from flashcard")
    suspend fun deleteAll()

}
