package cs.mad.flashcards.entities

import androidx.room.*

data class FlashcardSetContainer(
    val sets: List<FlashcardSet>
)

@Entity
data class FlashcardSet(
    val title: String
){
    @PrimaryKey (
        autoGenerate = true
    ) var id: Int = 0
}

@Dao
interface FlashcardSetDao {
    @Query("select * from flashcardset")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(set: FlashcardSet)

    @Insert
    suspend fun insertAll(sets: List<FlashcardSet>)

    @Update
    suspend fun update(set: FlashcardSet)

    @Delete
    suspend fun delete(set: FlashcardSet)

    @Query("delete from flashcardset")
    suspend fun deleteAll()
}
